﻿Uses Crt;
 Type
   vector = array[1..5] of real;
   matrix = array[1..5,1..5] of real;
 
 Const
   n:integer=5;
   e:real=power(10,-3);
 Var
   i,j,k,max:integer;
   Ek,sum1,sum2:real;
   a,alpha:matrix;
   b,beta,x_curr,x_prev,x_tmp:vector;
   
 BEGIN
 
   a[1,1]:=0.45;  a[1,2]:=0.03;   a[1,3]:=-0.01;   a[1,4]:=0.02;   a[1,5]:=-0.111;  b[1]:=-0.52;
   a[2,1]:=0.02;  a[2,2]:=0.375;  a[2,3]:=-0.01;   a[2,4]:=-0.01;  a[2,5]:=0; b[2]:=0.720;
   a[3,1]:=0;     a[3,2]:=0.07;   a[3,3]:=0.44;    a[3,4]:=0;      a[3,5]:=0.113;  b[3]:=2.34;
   a[4,1]:=-0.03; a[4,2]:=0.015;  a[4,3]:=-0.02;   a[4,4]:=0.41;   a[4,5]:=-0.084;  b[4]:=-1.680;
   a[5,1]:=0.02;      a[5,2]:=0.01; a[5,3]:=0;  a[5,4]:=0;     a[5,5]:=0.29;  b[5]:=-0;
 
Writeln('Вихідна матриця: ');
  for i:=1 to n do begin
  for j:=1 to n do
     write('  ',a[i,j]:1:3);
     writeln;
  end;
  
Writeln('Перетворена матриця: ');
  for i:=1 to n do begin
  for j:=1 to n do
     if i<>j then
  alpha[i,j]:=(-a[i,j])/(a[i,i])
          else
          alpha[i,j]:=0;
      end;
for i:=1 to n do begin
for j:=1 to n do
       write('  ',alpha[i,j]:1:3);
       writeln;
 end;
 
Writeln('Перевіримо достатню умову збіжності ітераційної послідовності: ' );
 for i:=1 to n do begin
 for j:=1 to n do
    Ek:=Ek+sqr(alpha[i,j]);
   end;
    Ek:=sqrt(Ek);
 
if Ek>1 then
        begin
      writeln ('Евклідова норма матриці ',Ek:1:3,'>1; умова не виконалася.');
        exit;
     end
      else
     begin
      writeln ('Евклідова норма матриці ',Ek:1:3,'<1; умова виконалася.');
   end;
 
Ek:=0; sum1:=0; sum2:=0; k:=0;
 
  for i:=1 to n do
    beta[i]:=b[i]/a[i,i];
   
  Repeat
    inc(k);
     if k <> 1 then
       for i:=1 to n do
         x_prev[i]:=x_tmp[i];
    for i:=1 to n do
      begin
         sum1:=0;
        for j:=1 to i-1 do
          sum1:=sum1 + a[i,j] * x_prev[j];
          sum2:=0;
        for j:=i+1 to n do
          sum2:=sum2 + a[i,j] * x_prev[j];
          x_curr[i]:=(b[i] - sum1 - sum2)/a[i,i];
      end;
    for i:=1 to n do
      x_tmp[i]:=x_curr[i];
      max:=1;
    for i:=1 to n-1 do
      if abs(x_curr[max]-x_prev[max]) < abs(x_curr[i+1]-x_prev[i+1]) then
         max:=i+1;
  Until  abs(x_curr[max]-x_prev[max]) < e;
       writeln('кількість ітерацій = ',k);
       writeln('корені системи: ');
    for i:=1 to n do
        write('  x[',i,']=',x_curr[i]:2:3);
    writeln('');
    writeln('Вектор нев"язки');
    var l:=1;
    for l:=1 to n do
        write(' V[',l,']=',  b[l]-x_curr[l]:2:3);
END.